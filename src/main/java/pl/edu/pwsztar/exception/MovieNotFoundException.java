package pl.edu.pwsztar.exception;

public class MovieNotFoundException extends Exception {
    public MovieNotFoundException(String message){
        super(message);
    }
    public MovieNotFoundException(){
        super();
    }
}
