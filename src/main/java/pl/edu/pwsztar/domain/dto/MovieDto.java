package pl.edu.pwsztar.domain.dto;

import java.io.Serializable;

public class MovieDto implements Serializable {
    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }

    private Long movieId;
    private String title;

    public void setTitle(String title) {
        this.title = title;
    }

    private String image;

    public void setImage(String image) {
        this.image = image;
    }

    private Integer year;

    public void setYear(Integer year) {
        this.year = year;
    }

    public MovieDto() {
    }

    public Long getMovieId() {
        return movieId;
    }

    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }

    public Integer getYear() {
        return year;
    }
}
